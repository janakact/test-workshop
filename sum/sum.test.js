const sum = require("./sum");

test("Sum of 10 and 20", () => {
  const result = sum(10, 20);
  expect(result).toBe(30);
});

test("Sum of 100 and 70", () => {
  expect(sum(100, 70)).toBe(170);
});
