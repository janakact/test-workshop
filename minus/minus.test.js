const minus = require("./minus");

test("Minus of 10 and 20", () => {
  const result = minus(10, 20);
  expect(result).toBe(-10);
});

test("Minus of 100 and 70", () => {
  expect(minus(100, 70)).toBe(30);
});
